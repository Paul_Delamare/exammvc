<section id="creneau">
    <div class="wrap">
        <h1><?php echo $creneau->title; ?></h1>
        <?php require ('form.php'); ?>
        <div class="listing_user_creneau">
           <h3>Tous les utilisateurs pour ce créneau</h3>
            <ul>
                <?php  foreach ($all_user_name as $name){
                    echo '<li><div class="nom">'.$name->nom.'</div><div class="enlever"><a href="'. $view->path('delete_creneau_user/'.$name->id_creneau.'/'.$name->id_user).'">Supprimer du créneau</a></div></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</section>