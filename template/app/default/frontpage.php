<section id="homepage">
    <div class="responsive1">
        <div class="wrap">
            <div class="bloc_1">
                <div class="listing_user">
                    <table>
                        <div class="responsive">
                            <thead>
                            <tr>
                                <th class="nom_user">Nom de l'utilisateur</th>
                                <th class="email_user">Email de l'utilisateur</th>
                            </tr>
                            </thead>
                            <caption><h2>Tous les utilisateurs</h2></caption>
                            <tbody>
                                <?php foreach ($users as $user){
                                    echo '<tr class="product">';
                                    echo    '<td>'.$user->nom.'</td>';
                                    echo    '<td class="email">'.$user->email.' </td>';
                                    echo '</tr>';
                                }  ?>
                            </tbody>
                        </div>
                    </table>
                </div>
                <div class="lisitng_salle">
                    <table>
                        <div class="responsive">
                            <thead>
                            <tr>
                                <th class="nom_user">Nom de la salle</th>
                                <th class="email_user">Nombre maximum de personnes</th>
                            </tr>
                            </thead>
                            <caption><h2>Toutes les salles disponibles</h2></caption>
                            <tbody>

                                <?php foreach ($salles as $salle){
                                    echo '<tr class="product">';
                                    echo    '<td>'.$salle->title.'</td>';
                                    echo    '<td class="email">'.$salle->maxuser.' </td>';
                                    echo '</tr>';
                                }  ?>
                            </tbody>
                        </div>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="responsive3">
        <div class="wrap">
            <div class="listing_creneau">
                <table>
                    <div class="responsive">
                        <thead>
                        <tr>
                            <th class="rang_3">Nom de la salle</th>
                            <th class="nom_user">Jour et heure de réservation</th>
                            <th class="email_user">Nombre d'heures reservé</th>
                            <th class="detail">Détails</th>
                        </tr>
                        </thead>
                        <caption><h2>Toutes les salles disponibles</h2></caption>
                        <tbody>
                            <?php foreach ($creneaux as $creneau){
                                echo '<tr class="product">';
                                echo    '<td class="rang_3">'.$creneau->title.'</td>';
                                echo    '<td>Le '.date('d/m/Y à H:i', strtotime($creneau->start_at)).'</td>';
                                echo    '<td class="email"> Pendant '.date('H:i', strtotime($creneau->nbrehours)).' heures</td>';
                                echo    '<td class="detail"><a href="'. $view->path('add_creneau_user/'.$creneau->id).'">Détails</a></td>';
                                echo '</tr>';
                            }  ?>
                        </tbody>
                    </div>
                </table>
            </div>
        </div>
    </div>
</section>