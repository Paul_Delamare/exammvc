<form method="post" action="">
    <div class="data">
        <?php echo $form->label('nom'); ?>
        <?php echo $form->input('nom'); ?>
        <?php echo $form->error('nom'); ?>
    </div>
    <div class="data">
        <?php echo $form->label('email'); ?>
        <?php echo $form->input('email', 'email'); ?>
        <?php echo $form->error('email'); ?>
    </div>
    <div class="submit">
        <?php echo $form->submit(); ?>
    </div>
</form>