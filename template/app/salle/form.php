<form method="post" action="">
    <div class="data">
        <?php echo $form->label('title'); ?>
        <?php echo $form->input('title'); ?>
        <?php echo $form->error('title'); ?>
    </div>
    <div class="data">
        <?php echo $form->label('max'); ?>
        <?php echo $form->input('max', 'max'); ?>
        <?php echo $form->error('max'); ?>
    </div>
    <div class="submit">
        <?php echo $form->submit(); ?>
    </div>
</form>