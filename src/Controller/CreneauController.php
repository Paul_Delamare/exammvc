<?php

namespace App\Controller;


use App\Model\CreneauModel;
use App\Model\CreneauUserModel;
use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;

class CreneauController extends BaseController {

    public function creneau_user($id){
        $error=[];
        if (!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v= new Validation();
            $error= $this->validate($v, $post, $id);
            if ($v->IsValid($error)){
                CreneauUserModel::addUserCreneau($id, $post);
                $this->redirect('home');
            }
        }
        $form= new Form($error);
        $creneau= $this->getCreneauByIdOr404($id);
        $users= UserModel::all();
        $all_user_creneau=CreneauUserModel::allUserCreneau($id);
        $this->render('app.creneau.creneau_user', array(
            'creneau'=>$creneau,
            'users'=>$users,
            'form'=>$form,
            'all_user_name'=>$all_user_creneau,
        ));
    }
    public function deleteUserCreneau($id_creneau, $id_user){
        $this->getUserCreneauByIdOr404($id_creneau, $id_user);
        CreneauUserModel::deleteUserCreneau($id_creneau, $id_user);
        $this->redirect('add_creneau_user/'.$id_creneau);
    }
    private function getCreneauByIdOr404($id){
        $creneau=CreneauModel::findByIdTitle($id);
        if (empty($creneau)){
            $this->Abort404();
        }
        return $creneau;
    }
    private function getUserCreneauByIdOr404($id_creneau, $id_user){
        $creneau=CreneauUserModel::existUserCreneau($id_creneau, $id_user);
        if (empty($creneau)){
            $this->Abort404();
        }
        return $creneau;
    }
    private function validate($v, $post, $id){
        $error=[];
        $error['user']=$v->existUser($post['user'], $post);
        $error['nombre']=$v->nombreUser($id);
        return $error;
    }
}