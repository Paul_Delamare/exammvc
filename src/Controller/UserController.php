<?php

namespace App\Controller;


use App\Model\CreneauModel;
use App\Model\CreneauUserModel;
use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;

class UserController extends BaseController{
    public function add(){
        $error=[];
        if (!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v= new Validation();
            $error=$this->validate($v, $post);
            if ($v->IsValid($error)){
                UserModel::addUser($post);
                $this->redirect('home');
            }
        }
        $form= new Form($error);
        $this->render('app.user.add_user', array(
            'form'=>$form,
        ));
    }
    private function validate($v, $post){
        $error=[];
        $error['nom']=$v->textValid($post['nom'], 'nom', 3, 255);
        $error['email']=$v->emailValid($post['email']);
        return $error;
    }

}