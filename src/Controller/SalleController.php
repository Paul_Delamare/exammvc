<?php

namespace App\Controller;


use App\Model\CreneauModel;
use App\Model\CreneauUserModel;
use App\Model\SalleModel;
use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;

class SalleController extends BaseController{

    public function add(){
        $error=[];
        if (!empty($_POST['submitted'])){
            $post= $this->cleanXss($_POST);
            $v= new Validation();
            $error= $this->validate($v, $post);
            if ($v->IsValid($error)){
                SalleModel::addSalle($post);
                $this->redirect('home');

            }
        }
        $form= new Form($error);
        $this->render('app.salle.add_salle',array(
            'form'=>$form,
        ));
    }

    private function validate($v, $post){
        $error=[];
        $error['title']=$v->textValid($post['title'], 'title', 3, 255);
        $error['max']=$v->textValid($post['max'], 'max', 3, 10000);
        return $error;
    }

}