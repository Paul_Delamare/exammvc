<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Model\UserModel;
use Core\Kernel\AbstractController;

/**
 *
 */
class DefaultController extends BaseController
{
    public function index()
    {
        $users= UserModel::all();
        $salles=SalleModel::all();
        $creneaux=CreneauModel::getTitleSalle();
        //$this->dump($message);
        $this->render('app.default.frontpage',array(
            'users' => $users,
            'salles'=>$salles,
            'creneaux'=>$creneaux,
        ));
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
