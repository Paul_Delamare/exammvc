<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class SalleModel extends AbstractModel{
    protected static $table = 'salle';
    protected string $title;
    protected string $maxuser;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getMaxuser(): string
    {
        return $this->maxuser;
    }

    public static function addSalle($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (title, maxuser) VALUES ( ? ,?)", array($post['title'], $post['max']));
    }



}