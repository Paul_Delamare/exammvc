<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class UserModel extends AbstractModel{
    protected static $table = 'user';
    protected string $nom;
    protected string $email;

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    public static function addUser($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (nom, email) VALUES ( ? ,?)", array($post['nom'], $post['email']));
    }
}