<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class CreneauUserModel extends AbstractModel{
    protected static $table = 'creneau_user';

    public static function addUserCreneau($idCreneau, $post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (id_creneau, id_user, created_at) VALUES ( ? ,?, NOW())", array($idCreneau, $post['user']));
    }
    public static function existCreneauUser($id, $post){
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE id_creneau = ? AND id_user = ?",[$id, $post['user']],get_called_class(),true);
    }
    public static function countUserCreneau($id){
        return App::getDatabase()->prepare("SELECT COUNT(id_user) AS user_nb FROM " . self::getTable() . " WHERE id_creneau = ?",array($id),get_called_class(), false);
    }
    public static function allUserCreneau($id){
        return App::getDatabase()->prepare("SELECT C.*, U.nom FROM " . self::getTable() . " C INNER JOIN ".UserModel::getTable() ." U ON C.id_user = U.id WHERE C.id_creneau = ?",array($id),get_called_class(), false);
    }
    public static function deleteUserCreneau($id_creneau, $id_user){
        return App::getDatabase()->prepareInsert("DELETE FROM " . self::getTable() . " WHERE id_creneau = ? AND id_user = ?",[$id_creneau, $id_user],get_called_class(),true);
    }
    public static function existUserCreneau($id_creneau, $id_user){
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE id_creneau = ? AND id_user = ?",[$id_creneau, $id_user],get_called_class(),true);
    }
}