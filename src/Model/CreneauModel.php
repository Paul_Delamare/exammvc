<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class CreneauModel extends AbstractModel{
    protected static $table = 'creneau';
    protected string $title;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }


    public static function getTitleSalle(){
        return App::getDatabase()->query("SELECT C.*, S.title FROM " . self::getTable() . " C INNER JOIN ".SalleModel::getTable() ." S ON C.id_salle = S.id ORDER BY C.start_at DESC",get_called_class());

    }

    public static function findByIdTitle($id,$columId = 'id'){
        return App::getDatabase()->prepare("SELECT C.*, S.title, S.maxuser FROM " . self::getTable() . " C INNER JOIN ".SalleModel::getTable()." S ON C.id_salle = S.id WHERE C.id = ?",[$id],get_called_class(),true);
    }


}