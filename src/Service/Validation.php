<?php
namespace App\Service;

use App\Model\CreneauModel;
use App\Model\CreneauUserModel;
use App\Model\UserModel;

class Validation
{
    protected $errors = array();

    public function IsValid($errors)
    {
        foreach ($errors as $key => $value) {
            if(!empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * emailValid
     * @param email $email
     * @return string $error
     */

    public function emailValid($email)
    {
        $error = '';
        if(empty($email) || (filter_var($email, FILTER_VALIDATE_EMAIL)) === false) {
            $error = 'Adresse email invalide.';
        }
        return $error;
    }

    /**
     * textValid
     * @param POST $text string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */

    public function textValid($text, $title, $min = 3,  $max = 50, $empty = true)
    {

        $error = '';
        if(!empty($text)) {
            $strtext = strlen($text);
            if($strtext > $max) {
                $error = 'Votre ' . $title . ' est trop long.';
            } elseif($strtext < $min) {
                $error = 'Votre ' . $title . ' est trop court.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }
        return $error;
    }
    public function existUser($id, $post){
        $error='';
        $existEmail=UserModel::findById($id);
        if (empty($existEmail)){
            $error='Cet utilisateur n\'existe pas';
        }else{
            $exist= CreneauUserModel::existCreneauUser($id, $post);
            if (!empty($exist)){
                $error='Cet utilisateur est déjà enregisté.';
            }
        }
        return $error;
    }
    public function nombreUser($id){
        $error='';
        if (CreneauUserModel::countUserCreneau($id)[0]->user_nb >= CreneauModel::findByIdTitle($id)->maxuser ){
            $error='Le nombre maximum d\'utilisateur à déjà était atteint';
        }
        return $error;
    }
}
